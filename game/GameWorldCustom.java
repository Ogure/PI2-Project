package game;

import java.util.List;

import joueurs.Joueur;
import joueurs.JoueurAction;

public class GameWorldCustom extends GameWorld {
    private int decalage = 0; // écart entre case à désactiver et dimensions du GameWorld
    private int seuil;
    private int tours = 0;

    GameWorldCustom(int dimx, int dimy, List<Joueur> jList, int jNum, boolean automatic, int tours, int seuil) {
	super(dimx, dimy, jList, jNum, automatic, tours);
	this.seuil = seuil;
	// TODO Auto-generated constructor stub
    }

    @Override
    void runGameWorld() {

	if (automatic)
	{
	    for (int i = 0; i < super.tours; i++)
	    {
		rapetissage();
		nextTurn();
		tours++;
	    }
	} else
	{
	    gInterface.automatic = false;
	    while (true)
	    {
		gInterface.getNextKey();
		System.out.println("Tours n°" + totalTours);
		nextTurn();
		tours++;
		rapetissage();

	    }
	}
    }

    @Override
    void nextTurn() {
	int vivant = 0;
	int jVivant = 0;

	for (int i = 1; i <= totalJoueurs; i++)
	{
	    if (tousJoueurs.get(i).getVivant())
	    {
		vivant++;
		jVivant = tousJoueurs.get(i).getID();
	    }
	    if (vivant > 1)
		break;
	}
	if (vivant == 1)
	{
	    System.out.println("Le vainqueur est J" + jVivant + " du niveau " + tousJoueurs.get(jVivant).getNiveau());
	    System.out.println("Pour trouver type d'IA : " + tousJoueurs.get(jVivant));
	    System.out.println("Il posédait dans son équipe : ");
	    for (int i = 0; i < tousJoueurs.get(jVivant).montreTroisPokemon().size(); i++)
	    {
		System.out.println("N°" + i + " " + tousJoueurs.get(jVivant).montreTroisPokemon().get(i).getNom()
			+ " lvl " + tousJoueurs.get(jVivant).montreTroisPokemon().get(i).getNiveau());
	    }
	    System.exit(0);
	} else if (vivant == 0)
	{
	    System.out.println("Vous êtes tous dummies...");
	    System.exit(0);
	}
	if (!(dimensionX % 2 == 1 && this.tours >= (int) dimensionX / 2 * seuil)) // Si il ne reste plus qu'une case, pas d'action possible direct combat
	{
	    for (int i = 1; i <= totalJoueurs; i++)
	    {
		if (!tousJoueurs.get(i).getVivant())
		    continue;
		Joueur j = tousJoueurs.get(i);
		JoueurAction a = tousJoueurs.get(i).nextAction();

		if (a == null)
		{
		    System.out.println(j.mort() + " Raison: rien d'action choisi");
		}

		String actDef = a.getActionDefinition();
		switch (a.getActionType()) {
		case move:
		    moveAction(j, actDef);
		    break;
		case fight:
		    fightAction(tousJoueurs.get(i), myGameWorld[j.getPositionX()][j.getPositionY()].getPokemon(actDef),
			    myGameWorld[j.getPositionX()][j.getPositionY()].terrain);
		    break;
		case capture:
		    captureAction(tousJoueurs.get(i),
			    myGameWorld[j.getPositionX()][j.getPositionY()].getPokemon(actDef));
		    break;
		case mort:
		    System.out.println(j.mort() + " Raison: Action mort choisir ?!");
		    break;
		}
	    }
	}
	for (Cellule[] cList : myGameWorld)
	{
	    for (Cellule c : cList)
	    {
		c.finTour();
	    }
	}
	gInterface.updateGameWorld(getGameWorldData());
	totalTours++;
    }

    void rapetissage() {
	// Il faut vérifier que l'on ne va pas rapetir la dernière case
	if (!(dimensionX % 2 == 1 && this.tours - 1 >= (int) dimensionX / 2 * seuil)) // S'arrete à une case restante // Si pas impaire ou pas dernière case
	{
	    if (!(dimensionX % 2 == 0 && this.tours - 1 >= ((int) dimensionX / 2 - 1) * seuil)) // Si pas paire et dernier 2*2
		if (tours % seuil == 0 && tours != 0) // Trois 'if' pour lisibilité
		{
		    // Gérer les joueurs :
		    decalageJoueurs();

		    // Désactivation des cellules
		    // Première colonne
		    for (int y = 0; y < dimensionY - decalage; y++)
		    {
			myGameWorld[decalage][y].active = false;
		    }
		    // Dernière colonne
		    for (int y = 0; y < dimensionY - decalage; y++)
		    {
			myGameWorld[dimensionX - decalage - 1][y].active = false;
		    }
		    // Première ligne
		    for (int x = 0; x < dimensionX - decalage; x++)
		    {
			myGameWorld[x][dimensionY - decalage - 1].active = false;
		    }
		    // Dernière ligne
		    for (int x = 0; x < dimensionX - decalage; x++)
		    {
			myGameWorld[x][decalage].active = false;
		    }
		    decalage++;
		}
	}
    }

    /**
     * Fonction permettant le décalage de chaque joueur pour ne pas qu'il se retrouve sur un cellule inactive
     */
    void decalageJoueurs() {
	int dimensionX = this.dimensionX - 1; // Plus propre car la dimension ne prend pas en compte le 0
	int dimensionY = this.dimensionY - 1;
	// Coins :
	// Bas à gauche :
	echangeJoueursCase(0 + decalage, 0 + decalage, 1 + decalage, 1 + decalage);

	// Haut à gauche
	echangeJoueursCase(0 + decalage, dimensionY - decalage, 1 + decalage, dimensionY - decalage - 1);

	// Haut à droite
	echangeJoueursCase(dimensionX - decalage, dimensionY - decalage, dimensionX - decalage - 1,
		dimensionY - decalage - 1);

	// Bas à droite
	echangeJoueursCase(dimensionX - decalage, 0 + decalage, dimensionX - decalage - 1, 0 + decalage + 1);

	// ********************************************************
	// Ligne du bas
	for (int i = 0; i < dimensionX; i++)
	{
	    echangeJoueursCase(0 + i, 0 + decalage, 0 + i, 1 + decalage); // Garde le meme X (pour chaque X) mais + 1 en Y
	}
	// Colonne de gauche
	for (int i = 0; i < dimensionY; i++)
	{
	    echangeJoueursCase(0 + decalage, 0 + i, 1 + decalage, 0 + i);
	}
	// Ligne du haut
	for (int i = 0; i < dimensionX; i++)
	{
	    echangeJoueursCase(0 + i, dimensionY - decalage, 0 + i, dimensionY - decalage - 1);
	}
	// Colonne de droite
	for (int i = 0; i < dimensionY; i++)
	{
	    echangeJoueursCase(dimensionX - decalage, 0 + i, dimensionX - decalage - 1, 0 + i);
	}
    }

    /**
     * Déplace les joueurs d'une case en x/y en x_current/y_current
     * 
     * @param x
     *            Coordonnée x de la case avant déplacement
     * @param y
     *            Coordonnée y de la case avant déplacement
     * @param x_current
     *            Coordonnée x de la case après déplacement
     * @param y_current
     *            Coordonnée y de la case après déplacement
     */
    void echangeJoueursCase(int x, int y, int x_current, int y_current) {
	for (int i = 0; i < myGameWorld[x][y].joueurs.size(); i++)
	{
	    if (myGameWorld[x][y].joueurs.get(i).getVivant())
	    {
		myGameWorld[x][y].joueurs.get(i).updatePosition(x_current, y_current);
		myGameWorld[x_current][y_current].joueurs.add(myGameWorld[x][y].joueurs.get(i));
		myGameWorld[x][y].joueurs.remove(i);
		i--; // Car il y a un joueur de moins dans la liste
	    }
	}
    }

}
