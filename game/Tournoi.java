package game;

import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;

import joueurs.*;
import joueurs.Joueur;

/**********************************************************************************
 * Class Tournoi TODO
 **********************************************************************************/
/**
 * @author Paul-Elie Pipelin
 *
 */
class Tournoi {

    /**
     * A la fin de chaque tour, pour chaque cellule, tous les joueurs vivants présents à l'intérieur d'une même cellule participent à un "tournoi" qui determine le vainqueur qui restera dans la cellule. Les autres joueurs sont éliminés.
     * <ul>
     * <li>Phase 1 Seeding
     * <ol>
     * <li>Le classement d'un joueur est calculé grâce à la somme des niveaux des trois meilleurs pokemons de chaque joueur</li>
     * <li>La liste des joueurs est triée en utilisant les classements du joueur</li>
     * <li>Si le nombre de joueurs n'est pas une puissance de 2, il faut ajouter quelques DummyJoueurs sans pokemon à la fin de la liste jusqu'à obtenir un nombre de joueurs égal à une puissance de 2</li>
     * <li>la sequence de combats est decidée par les règles de seeding. Il faut mettre les joueurs dans une queue (java.util.Queue) pour définir la séquence de combats (Pour la définition du seeding, reporter à https://en.wikipedia.org/wiki/Seed_(sports))</li>
     * </ol>
     * </li>
     * <li>Phase 2 Bataille
     * <ol>
     * <li>les joueurs se battent par paires sous les regles de "seeding"
     * <ul>
     * <li>le joueur avec le plus de points (première phase) combat celui qui en a le moins</li>
     * <li>le deuxieme joueur avec le plus de points combat le deuxieme qui en a le moins</li>
     * <li>... et etc.</li>
     * <li>le vainqueur continue le sous-tournoi, l'autre est éliminé</li>
     * </ul>
     * </li>
     * <li>Utiliser fight(Joueur j1, Joueur j2) pour determiner le vainqueur</li>
     * <li>Le tournoi se termine quand il ne reste plus qu'un joueur</li>
     * </ol>
     * </li>
     * </ul>
     * 
     * @return retourne l'ID du meilleur joueur
     * @param joueurs
     *            liste des joueurs dans la cellule (ne se limite pas aux vivants)
     * @param terrain
     *            type du terrain
     */
    static int faireTournoi(List<Joueur> joueurs, String terrain) {
	// Trie les joueurs par niveau (des trois plus forts pkmn)
	joueurs = triage(joueurs);
	if (joueurs.size() == 0 || joueurs.size() == 1)
	    return -1;
	// Car les combat sont appelés en permanence évite de créer des DummyJoueurs dans une case vide
	int pow2 = 1;
	// Recherche quelle puissance de 2 est supérieure (ou égale) à joueurs.size()
	while (pow2 < joueurs.size())
	{
	    pow2 *= 2;
	}
	// Incrémente la taille de la liste jusqu'à arriver à la-dite puissance pow2
	while (joueurs.size() < pow2)
	{
	    joueurs.add(new DummyJoueur(0));
	}

	Queue<Joueur> queue = new ArrayBlockingQueue<>(pow2);
	// Création de la queue pour les combats

	// Implémentation de du remplissage de queue par rapport au trie placeur:
	Map<Integer, Joueur> numJoueurs = new HashMap<>();
	// La clé est la future place du joueur dans la queue
	// Position des joueurs et leur nombre :
	// System.out.println("Size de joueurs en " + joueurs.get(0).getPositionX() + "/" + joueurs.get(0).getPositionX() + " : " + joueurs.size());
	for (int i = 1; i <= joueurs.size(); i++)
	{
	    numJoueurs.put(placeur(i, joueurs.size()), joueurs.get(i - 1)); // Dépose le premier càd le 0
	    // ***********************************************
	    // System.out.println("Met joueur n°" + (i - 1) + " en position " + placeur(i, joueurs.size()));
	    // ***********************************************
	}
	// Parcours la Map pour remplir la queue
	for (int i = 0; !numJoueurs.isEmpty() && i < joueurs.size(); i++)
	{
	    queue.add(numJoueurs.get(i));
	}
	// J'aurais pu uiliser un système de removes pour toutes ces parties avec des list, map etc. mais je fais confiance au ramasse-miettes
	// Lancement des combats :
	while (queue.size() > 1)
	{
	    Joueur j1 = queue.poll();
	    Joueur j2 = queue.poll();
	    if (j1.montreTroisPokemon() != null && j2.montreTroisPokemon() != null)
	    {
		//System.out.println("Lance combat de " + j1.getID() + " contre " + j2.getID() + " en " + j1.getPositionX() + "/" + j1.getPositionX());
		int perdant = fight(j1, j2, terrain);
		if (perdant == 2)
		    j2.mort();
		else
		    j1.mort();
		queue.add(perdant == 2 ? j1 : j2);
	    }
	}
	return queue.peek().getID();
    }

    /**
     * Place un joueur dans une seed de taille pow2 (à chaque round, la seed garde une forme de seed) : 1/5 3/7 || 2/6 4/8 devient 1/3 | 2/4
     * 
     * @param rang
     *            Rang du joueur par rapport aux niveaux de ses 3 meilleurs pokemons
     * @param pow2
     *            Nombre de participants (puissance de 2)
     * @return retourne la position de base du joueur selon une seed (0 pour le premier)
     */
    public static int placeur(int rang, int pow2) {
	// Cas de base
	if (rang <= 1)
	{
	    return 0;
	}

	// Plaçage à droite
	// Appel réccursif avec la moitié du rang et la moitié de la puissance de 2
	if (rang % 2 == 0)
	{
	    return pow2 / 2 + placeur(rang / 2, pow2 / 2);
	}

	// Plaçage à gauche
	// Vu que impaire : reste impaire
	return placeur(rang / 2 + 1, pow2 / 2);
    }

    /**
     * Trie une liste de joueurs
     * 
     * @param list
     *            liste de Joueur
     * @return Liste triée des joueurs de la liste list par rapport aux niveaux de leur 3 meilleurs pokemons
     */
    static List<Joueur> triage(List<Joueur> list) {
	List<Joueur> retour = new ArrayList<>();
	for (int compteurList = 0; compteurList < list.size(); compteurList++)
	{
	    if (list.get(compteurList).getVivant())
	    {
		if (retour.isEmpty())
		{
		    retour.add(list.get(compteurList)); // Premier joueur
		} else
		{
		    int compteurRetour = 0;
		    // Parcours la liste de retour
		    while (compteurRetour < retour.size()
			    && nivTot(list.get(compteurList).montreTroisPokemon()) > nivTot(
				    retour.get(compteurRetour).montreTroisPokemon()))
		    // Va juqu'au bout de la liste triée ou
		    // S'arrete si il y'a une place où mettre le joueur
		    {
			compteurRetour++;
		    }
		    retour.add(compteurRetour, list.get(compteurList));
		    // Ici on profite du fait que les liste permette l'insertion et le décalement automatique
		}
	    }
	}
	return retour;
    }

    /**
     * Calcul la somme des niveaux d'au plus trois pokemon de la liste pokemons et la renvoie
     * 
     * @param pokemons
     *            Liste des 3 meilleurs pokemons (de la fonction MontreTroisPokemon par exemple)
     * @return somme des niveuax des 3 plus forts pokemons de la liste pokemons
     */
    static int nivTot(List<Pokemon> pokemons) {
	int retour = 0;
	if (!(pokemons.isEmpty()))
	{
	    for (int i = 0; i < pokemons.size() && i < 3; i++)
	    {
		retour += pokemons.get(i).getNiveau();
	    }
	}
	return retour;
    }

    /**
     * Calcul les différents bonus du pokémon p sur le pokémon p2 Si le type de p est avantageux sur p2 alorscette fonction renvoie le montant du bonus renvoie 0.0 sinon
     * 
     * @param p
     *            pokémon attaquant
     * @param p2
     *            pokémon attaqué
     * @param terrain
     *            type de la cellule où se passe le combat
     * @return Bonus du terrain (0.0 si aucun bonus)
     */
    static Double calculBonus(Pokemon p, Pokemon p2, String terrain) {
	double bonusTerrain = 0.0;
	if (terrain == p.getType())
	    bonusTerrain = 1.0;
	if (p.getType() == "Eau" && p2.getType() == "Feu")
	    return (0.1 * p2.getNiveau() + 1.0) + bonusTerrain;
	if (p.getType() == "Plante" && p2.getType() == "Eau")
	    return (0.1 * p2.getNiveau() + 1.0) + bonusTerrain;
	if (p.getType() == "Feu" && p2.getType() == "Plante")
	    return (0.1 * p2.getNiveau() + 1.0) + bonusTerrain;
	return 0.0 + bonusTerrain;
    }

    /**
     * Calcul de l'expériencegagnée suite à un combat
     * 
     * @param p
     *            Pokemon gagnant
     * @param p2
     *            Pokemon perdant
     * @param egalite
     *            si il y'a égalité
     */
    static void calculExp(Pokemon p, Pokemon p2, boolean egalite) {
	if (!egalite)
	{
	    // Gagnant :
	    p.addPoints(1.0 + 0.1 * p2.getNiveau());
	    // Perdant :
	    p2.addPoints(0.1 + 0.1 * p.getNiveau());
	} else
	{
	    p.pointsXP += 0.5;
	    p2.pointsXP += 0.5;
	}
    }

    /**
     * <p>
     * Bataille entre deux pokémon : le pokemon p1 et le Pokemon p2 Si un pokemon est null, l'adversaire est le vainqueur. Si les deux sont null, p1 est le vainqueur.
     * <ul>
     * <li>PHASE 1 : calcul de l'attaque de chaque pokémon
     * <ul>
     * <li>l'attaque du pokemon est determinée par la formule suivante : niveau du pokemon + random bonus d'un double entre 0 et 2 + bonus type + bonus terrain</li>
     * <li>calcule du bonus type: 10% bonus sur niveau du pokemon de l'adversaire + 1.0 d'attack pour feu sur herbe, herbe sur eau, et eau sur feu</li>
     * <li>calcule du bonus terrain: les pokemons du meme type du terrain gagnent +1.0 d'avantage</li>
     * </ul>
     * </li>
     * <li>PHASE 2 : Bataille
     * <ul>
     * <li>chaque bataille se compose de 3 tours entre les deux pokemon, et le vainqueur d'un tour est le pokemon avec la meilleure valeur d'attaque</li>
     * <li>l'attaque est recaculée pour chaque tour</li>
     * <li>le pokemon qui gagne le plus de tours est le vainqueur</li>
     * </ul>
     * </li>
     * <li>PHASE 3 : Calcul des points d'experience
     * <ul>
     * <li>le pokemon vainqueur gagne 1.0 point XP + 10% bonus du niveau de l'adversaire</li>
     * <li>le perdant gagne 0.1 point, plus 10% bonus du niveau de l'adversaire</li>
     * <li>si les deux pokémon sont à égalité, chaque pokémon gagne 0.5 points XP (et pas de bonus)</li>
     * </ul>
     * </li>
     * </ul>
     * 
     * @return le perdant, 1 pour p1, 2 pour p2, et -1 en cas d'égalité (ou si deux joueurs sont null)
     * @param p1
     *            pokemon du j1
     * @param p2
     *            pokemon sauvage ou pokemon du j2
     * @param terrain
     *            type du terrain ("Feu", "Eau", ou "Herbe")
     */
    static int bataillePokemon(Pokemon p1, Pokemon p2, String terrain) {
	int perdant = -1;
	if (p1 != null && p2 != null)
	{
	    //System.out.println("Combat pkmn de " + p1.getNom() + " de lvl " + p1.getNiveau() + " contre " + p2.getNom() + " de lvl " + p2.getNiveau());
	    double bonusP1 = calculBonus(p1, p2, terrain);
	    double bonusP2 = calculBonus(p2, p1, terrain);
	    int vP1 = 0, vP2 = 0;
	    for (int i = 0; i < 3; i++)
	    {
		double attaqueP1 = p1.getNiveau() + new Random().nextDouble() * 2 + bonusP1;
		double attaqueP2 = p2.getNiveau() + new Random().nextDouble() * 2 + bonusP2;
		if (attaqueP1 > attaqueP2)
		    vP1++;
		else if (attaqueP1 < attaqueP2)
		    vP2++;
	    }
	    if (vP1 > vP2)
	    {
		calculExp(p1, p2, false);
		perdant = 2;
	    } else if (vP1 < vP2)
	    {
		calculExp(p2, p1, false);
		perdant = 1;
	    } else
		calculExp(p1, p2, true);
	} else if (p1 == null && p2 != null)
	    perdant = 1;
	else if (p2 == null && p1 != null)
	    perdant = 2;
	return perdant;
    }

    /**
     * TODO
     * <p>
     * prérequis : les deux joueurs ne sont pas null
     * <ol>
     * <li>prepare les deux joueurs en leur donnant le type du meilleur pokemon de l'adversaire en utilisant Joueur.prepareBattle(String) par exemple Joueur.prepareBattle("Eau")</li>
     * <li>prend les pokemons choisis par chaque joueur</li>
     * <li>lance la bataille</li>
     * <li>si deux joueurs sont à égalité, relance la bataille jusqu'à obtenir un vainqueur</li>
     * <li>élimine le joueur perdant</li>
     * </ol>
     * 
     * @return retourne le perdant, 1 pour j1, 2 pour j2
     * @param j1
     *            premier joueur
     * @param j2
     *            deuxième joueur
     * @param terrain
     *            type du terrain ("Feu", "Eau", ou "Herbe")
     */
    static int fight(Joueur j1, Joueur j2, String terrain) {
	int perdant;
	j2.prepareBattle(j1.getStrongestPokemonType());
	j1.prepareBattle(j2.getStrongestPokemonType());
	perdant = bataillePokemon(j1.choisirPokemon(), j2.choisirPokemon(), terrain);
	//System.out.println("Perdant : " + (perdant == 1 ? j1.getID() : perdant == 2 ? j2.getID() : ""));
	if (perdant == 1)
	{
	    return 1;
	} else
	{
	    return 2;
	}
    }

    /**
     * TODO Un joueur qui essaie de capturer un pokemon. Si le niveau du Joueur + un réel aléatoire entre 0 et 2 est superieur ou égal au niveau de p plus un réel aléatoire entre 0 et 1, la capture est un succès Si le pokemon p est null, c'est faux Si la capture est un succès, donne une copie du pokémon au joueur (p.copy())
     * 
     * @return si la capture est un succès
     * @param j
     *            joueur
     * @param p
     *            pokemon que le joueur désire capturer
     */
    static boolean capture(Joueur j, Pokemon p) {
	boolean captureOk = p != null
		&& j.getNiveau() + new Random().nextDouble() * 2 > p.getNiveau() + new Random().nextDouble();
	if (captureOk)
	{
	    j.ajoutePokemon(p.copy());
	    p.capture();
	}
	return (captureOk);
    }
}
