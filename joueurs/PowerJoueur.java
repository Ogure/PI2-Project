package joueurs;

import java.util.*;

import game.GameWorld;
import game.Pokemon;
import game.TestPokemonJava;

public class PowerJoueur extends Joueur {
    private int niveau;

    int samePlace;
    final int tauxDeChangement = 5;

    /**
     * Constructeur de PowerJoueur
     * 
     * @param ID
     *            id du joueur
     */
    public PowerJoueur(int ID) {
	mesPokemons = new ArrayList<>();
	// Pas de bonbon pour cette IA
	vivant = true;
	id = ID;
    }

    @Override
    public void ajoutePokemon(Pokemon p) {
	mesPokemons.add(p);
	trie();
    }

    public void trie() {
	// Créer un comparateur pour trier la liste par rapport aux niveaux.
	Comparator<Pokemon> nivSup = (Pokemon a, Pokemon b) -> {
	    return (b.getNiveau() - a.getNiveau());
	};
	Collections.sort(mesPokemons, nivSup);
    }

    @Override
    public Pokemon choisirPokemon() {
	if (mesPokemons.isEmpty())
	    return null;
	else
	    return mesPokemons.get(0);
    }

    @Override
    public Joueur copy(int id) {
	// Rend une copie du PowerJoueur
	return new PowerJoueur(id);
    }

    @Override
    public int getNiveau() {
	return this.niveau;
    }

    @Override
    public String getStrongestPokemonType() {
	if (!vivant)
	    return "Mort";
	else if (mesPokemons.size() > 0)
	{
	    String typeDominant = "";
	    int lvlMax = 0;
	    for (int compteur = 0; compteur < mesPokemons.size(); compteur++)
	    {
		if (mesPokemons.get(compteur).getNiveau() >= lvlMax)
		    typeDominant = mesPokemons.get(compteur).getType();
	    }
	    return typeDominant;
	}
	return "";
    }

    @Override
    public JoueurAction nextAction() {
	boolean pkmnPres = false;
	String aDef = "";
	String aType = "";
	String pkmnNom = "";
	Random rand = new Random();

	GameWorld.CelluleData currentCell = TestPokemonJava.getGameWorldData().get(getPositionX()).get(getPositionY()); // Pour obtenir les cellules

	for (int i = 0; i < currentCell.cellCreatures.size(); i++)
	{
	    if (currentCell.cellCreatures.get(i).nom.charAt(0) == 'P')
	    {
		pkmnPres = true;
		for (int y = 1; y < currentCell.cellCreatures.get(i).nom.length(); y++)
		{
		    pkmnNom += currentCell.cellCreatures.get(i).nom.charAt(y);
		}
		break;
	    }
	}
	if (pkmnPres && samePlace < tauxDeChangement)
	{
	    samePlace++;
	    // Cas "fight";
	    aType = "fight";
	    aDef = pkmnNom;
	} else
	{
	    samePlace = 0;
	    // Cas "move"
	    aType = "move";
	    List<List<GameWorld.CelluleData>> cellData = TestPokemonJava.getGameWorldData();
	    while (aDef == "")
	    {
		switch (rand.nextInt(4)) {
		case 0:
		    if (getPositionX() > 0 && cellData.get(getPositionX() - 1).get(getPositionY()).active) // premiere vérification nécésdsaire pour ne pas avoir de null
			aDef = "Left";
		    break;
		case 1:
		    if (getPositionX() < cellData.size() - 1
			    && cellData.get(getPositionX() + 1).get(getPositionY()).active)
			aDef = "Right";
		    break;
		case 2:
		    if (getPositionY() < cellData.get(getPositionX()).size() - 1
			    && cellData.get(getPositionX()).get(getPositionY() + 1).active)
			aDef = "Up";
		    break;
		case 3:
		    if (getPositionY() > 0 && cellData.get(getPositionX()).get(getPositionY() - 1).active)
			aDef = "Down";
		    break;
		}
	    }
	}
	this.niveau = mesPokemons.get(0).getNiveau();
	return new JoueurAction(aType, aDef);
    }

    /**
     * Echange la place de deux pokémons dans la liste mesPokemons. Ancien Prérequis fixed : le pokemon p est AVANT le pokemon p2 dans la liste
     * 
     * @param p
     *            place dans mesPokemons du premier pokemon à échanger(base 0)
     * @param p2
     *            place du second
     */
    public void echangePkmn(int p, int p2) {
	if (!(p == p2))
	{
	    Pokemon tmp = mesPokemons.get(p);
	    Pokemon tmp2 = mesPokemons.get(p2);
	    mesPokemons.remove(p);
	    mesPokemons.add(p, tmp2);
	    mesPokemons.remove(p2);
	    mesPokemons.add(p2, tmp);
	}
    }

    @Override
    public boolean getVivant() {
	return vivant;
    }

    @Override
    public String mort() {
	vivant = false;
	return "PowerJoueur " + id + " disqualified.";
    }

    @Override
    public void prepareBattle(String type) {
	// Useless
    }

    @Override
    public List<Pokemon> montreTroisPokemon() {
	List<Pokemon> retour = new ArrayList<>();
	retour.add(mesPokemons.get(0));
	return retour;
    }

    @Override
    public void creerBonbon() {
	// TODO Auto-generated method stub

    }

    @Override
    public void utiliseBonbon() {
	// TODO Auto-generated method stub

    }
}
